#include <ros/ros.h>
#include <std_msgs/String.h>

#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/PoseArray.h>

using namespace cv;
using namespace std;

//***********************************
int threshold_line = 30;
int min_line_length = 45;
int max_line_gap = 3;


double orientation;
ros::Publisher chatter_pub;

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_ptr;
  bool found = false;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    cv::Mat image_mat = cv_ptr->image;

    Mat gray;

    cvtColor(image_mat, gray, COLOR_BGR2GRAY); //Convert the captu frame from BGR to HSV
    
    Mat canny_output;

    /// Detect edges using canny
    Canny( gray, canny_output, 95, 200, 3 );

    vector<vector<Point> > contours;

    vector<Vec4i> hierarchy;


    /// Find contours 
    findContours( canny_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

    vector<Rect> boundRect( contours.size() );
    vector<vector<Point> > contours_poly_( contours.size() );
    
      /// Find the rotated rectangles and ellipses for each contour
    vector<RotatedRect> minRect( contours.size() );
    vector<RotatedRect> minEllipse( contours.size() );

    for( int i = 0; i < contours.size(); i++ ){ 
      minRect[i] = minAreaRect( Mat(contours[i]) );
      if( contours[i].size() > 5 )
        { minEllipse[i] = fitEllipse( Mat(contours[i]) ); }
    }


    for( int i = 0; i< contours.size(); i++ )
    {
      Scalar color = Scalar( 0,0,255);
      //drawContours( image_mat, contours, i, color, 2, 8, hierarchy, 0, Point() );
       
      ellipse( image_mat, minEllipse[i], color, 2, 8 );
    }


    //imshow("canny_output ",imgThresholded);
    imshow("canny", canny_output);
    imshow("original", image_mat);

  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
  cv::waitKey(0);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "image_listener");
  ros::NodeHandle nh;


  cv::startWindowThread();
  image_transport::ImageTransport it(nh);
  //image_transport::Subscriber sub = it.subscribe("/softkinetic_camera/rgb/image_color", 1, imageCallback);
  image_transport::Subscriber sub = it.subscribe("/spqr_camera/rgb", 1, imageCallback);

  chatter_pub = nh.advertise<geometry_msgs::PoseArray>("boxes", 1000);

  ros::spin();
}


